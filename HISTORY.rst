=======
History
=======

0.2.0 (2018-03-09)
------------------

New features:

* APFS/Darwin support
* Filenames outside ascii are supported (thank you, Kriskras99)

Bug fixes:

* Building on newer glibc (thank you, Frankie Dintino)
* Better error handling for ENOENT

Other changes:

* Continuous integration for Windows (AppVeyor), integrated in the GitLab pipeline

0.1.0 (2017-07-27)
------------------

* First release on PyPI.
